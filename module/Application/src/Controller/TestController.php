<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TestController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    private function findXNo1($lengthOfX)
    {
        // No.1
        //3, 5, 9, 15, X
        $temp = 3;
        $result = array();
        for ($i=0; $i < $lengthOfX; $i++) {
            $temp += $i*2;
            array_push($result,$temp);
        }
        return $result;
    }
    private function findYNo2()
    {
        // No.2
        // (Y + 24)+(10 × 2) = 99 
        $y = 99-(10*2)-24;
        return $y;
    }
    private function findXNo3($position)
    {
         // No.3 
         // If 1 = 5 , 2 = 25 , 3 = 325 , 4 = 4325 Then 5 = X
        $temp = 0;
        for ($i=0; $i < $position; $i++) { 
            $temp = ($i==0)?4:$temp;
            $pow = pow(10,$i) * ($i+1);
            $temp = $temp+$pow;
        }
        return $temp;
    }
}
